FROM golang:1.16
ENV GO111MODULE=auto

RUN go get -d -v golang.org/x/net/html  
COPY app.go ./
RUN CGO_ENABLED=0 go build -a -installsuffix cgo -o app .

CMD ["./app"]
